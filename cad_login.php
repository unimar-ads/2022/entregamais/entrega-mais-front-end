<?php
session_start();
include('conexao.php');
?>

<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <title>Entrega+</title>
</head>

<body style="min-width:372px;">
    <nav class="navbar navbar-expand-lg navbar-dark bg-danger border-bottom shadow-sm mb-3">
        <div class="container">
            <a class="navbar-brand" href="index.php"><img src="img/logo.jpg" width="100"><strong> Entrega+</strong></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="nav justify-content-center">
                <div class="align-self-end">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        <a class="btn btn-light" href="login.php" role="button">Entrar</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="login">
        <div class="container">
		<div class="container-login">
			<div class="wrap-login">
				<form class="login-form" action="cad.php" method="POST">
					<span class="login-form-title">
						Criar Conta
					</span>

					<div class="wrap-input margin-top-35 margin-bottom-35">
						<input class="input-form" type="text" name="name" autocomplete="off">
						<span class="focus-input-form" data-placeholder="Nome"></span>
					</div>

          <div class="wrap-input margin-top-35 margin-bottom-35">
						<input class="input-form" type="text" name="email" autocomplete="off">
						<span class="focus-input-form" data-placeholder="E-mail"></span>
					</div>

          <div class="wrap-input margin-bottom-35">
						<input class="input-form" type="password" name="password">
						<span class="focus-input-form" data-placeholder="Senha"></span>
					</div>

          <div class="wrap-input margin-bottom-35">
						<input class="input-form" type="password" name="">
						<span class="focus-input-form" data-placeholder="Confirmação de senha"></span>
					</div>

					<div class="container-login-form-btn">
                        <button class="btn btn-light login-form-btn" type="submit">Criar</button>
					</div>
				</form>
			</div>

		</div>
	</div>
    

    <footer class="border-top fixed-bottom text-muted bg-light">
        <div class="container">
            <div class="row py-3">
                <div class="col-12 col-md-4 text-center text-md-left">
                    &copy; 2022 - Entrega+
                </div>
                <div class="col-12 col-md-4 text-center">
                    <a href="privacidade.php" class="text-decoration-none text-dark">Política de Privacidade</a>
                </div>
                <div class="col-12 col-md-4 text-center text-md-right">
                <a href="termos.php" class="text-decoration-none text-dark">Termos de Uso</a>
                </div>
            </div>
        </div>
    </footer>

    
</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>