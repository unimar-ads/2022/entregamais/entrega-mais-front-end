<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Entrega+</title>
</head>

<body style="min-width:372px;">
    <nav class="navbar navbar-expand-lg navbar-dark bg-danger border-bottom shadow-sm mb-3">
        <div class="container">
            <a class="navbar-brand" href="index.php"><img src="img/logo.jpg" width="100"><strong> Entrega+</strong></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="nav justify-content-center">
                <div class="align-self-end">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        <a class="btn btn-light" href="inicio.php" role="button">Voltar</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <h4>1. Termos de Uso</h4>
        <br>
        <p><span>Ao acessar ao site <a href="www.entrega+.com">Entrega +</a>, concorda
                em cumprir estes termos de serviço, todas as leis e regulamentos aplicáveis ​​e concorda que é
                responsável pelo cumprimento de todas as leis locais aplicáveis. Se você não concordar com algum desses
                termos, está proibido de usar ou acessar este site. Os materiais contidos neste site são protegidos
                pelas leis de direitos autorais e marcas comerciais aplicáveis.</span></p>
        <br>
        <h4>2. Uso de Licença</h4>
        <br>
        <p><span>É concedida permissão para baixar temporariamente uma cópia dos
                materiais (informações ou software) no site Entrega + , apenas para visualização transitória pessoal e
                não comercial. Esta é a concessão de uma licença, não uma transferência de título e, sob esta licença,
                você não pode:&nbsp;</span></p>
        <br>
        <ol>
            <li><span>modificar ou copiar os materiais;&nbsp;</span></li>
            <li><span>usar os materiais para qualquer finalidade comercial ou para
                    exibição pública (comercial ou não comercial);&nbsp;</span></li>
            <li><span>tentar descompilar ou fazer engenharia reversa de qualquer
                    software contido no site Entrega +;&nbsp;</span></li>
            <li><span>remover quaisquer direitos autorais ou outras notações de
                    propriedade dos materiais; ou&nbsp;</span></li>
            <li><span>transferir os materiais para outra pessoa ou 'espelhe' os
                    materiais em qualquer outro servidor.</span></li>
        </ol>
        <p><span>Esta licença será automaticamente rescindida se você violar alguma
                dessas restrições e poderá ser rescindida por Entrega + a qualquer momento. Ao encerrar a visualização
                desses materiais ou após o término desta licença, você deve apagar todos os materiais baixados em sua
                posse, seja em formato eletrónico ou impresso.</span></p>
        <br>
        <h4>3. Isenção de responsabilidade</h4>
        <br>
        <ol>
            <li><span>Os materiais no site da Entrega + são fornecidos 'como estão'.
                    Entrega + não oferece garantias, expressas ou implícitas, e, por este meio, isenta e nega todas as
                    outras garantias, incluindo, sem limitação, garantias implícitas ou condições de comercialização,
                    adequação a um fim específico ou não violação de propriedade intelectual ou outra violação de
                    direitos.</span></li>
            <li><span>Além disso, o Entrega + não garante ou faz qualquer
                    representação relativa à precisão, aos resultados prováveis ​​ou à confiabilidade do uso dos
                    materiais em seu site ou de outra forma relacionado a esses materiais ou em sites vinculados a este
                    site.</span></li>
        </ol>
        <br>
        <h4>4. Limitações</h4>
        <br>
        <p><span>Em nenhum caso o Entrega + ou seus fornecedores serão responsáveis
                ​​por quaisquer danos (incluindo, sem limitação, danos por perda de dados ou lucro ou devido a
                interrupção dos negócios) decorrentes do uso ou da incapacidade de usar os materiais em Entrega +, mesmo
                que Entrega + ou um representante autorizado da Entrega + tenha sido notificado oralmente ou por escrito
                da possibilidade de tais danos. Como algumas jurisdições não permitem limitações em garantias
                implícitas, ou limitações de responsabilidade por danos conseqüentes ou incidentais, essas limitações
                podem não se aplicar a você.</span></p>
        <br>
        <h4>5. Precisão dos materiais</h4>
        <br>
        <p><span>Os materiais exibidos no site da Entrega + podem incluir erros
                técnicos, tipográficos ou fotográficos. Entrega + não garante que qualquer material em seu site seja
                preciso, completo ou atual. Entrega + pode fazer alterações nos materiais contidos em seu site a
                qualquer momento, sem aviso prévio. No entanto, Entrega + não se compromete a atualizar os
                materiais.</span></p>
        <br>
        <h4>6. Links</h4>
        <br>
        <p><span>O Entrega + não analisou todos os sites vinculados ao seu site e não
                é responsável pelo conteúdo de nenhum site vinculado. A inclusão de qualquer link não implica endosso
                por Entrega + do site. O uso de qualquer site vinculado é por conta e risco do usuário.</span></p>
        <p><br></p>
        <br>
        <h3>Modificações</h3>
        <br>
        <p><span>O Entrega + pode revisar estes termos de serviço do site a qualquer
                momento, sem aviso prévio. Ao usar este site, você concorda em ficar vinculado à versão atual desses
                termos de serviço.</span></p>
        <br>
        <h3>Lei aplicável</h3>
        <br>
        <p><span>Estes termos e condições são regidos e interpretados de acordo com as
                leis do Entrega + e você se submete irrevogavelmente à jurisdição exclusiva dos tribunais naquele estado
                ou localidade.</span></p>
        <br>
        <br>
        <br>
        <br>
    </div>
    <footer class="border-top fixed-bottom text-muted bg-light">
        <div class="container">
            <div class="row py-3">
                <div class="col-12 col-md-4 text-center text-md-left">
                    &copy; 2022 - Entrega+
                </div>
                <div class="col-12 col-md-4 text-center">
                    <a href="privacidade.php" class="text-decoration-none text-dark">Política de Privacidade</a>
                </div>
                <div class="col-12 col-md-4 text-center text-md-right">
                <a href="termos.php" class="text-decoration-none text-dark">Termos de Uso</a>
                </div>
            </div>
        </div>
    </footer>



</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>