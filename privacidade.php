<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Entrega+</title>
</head>

<body style="min-width:372px;">
    <nav class="navbar navbar-expand-lg navbar-dark bg-danger border-bottom shadow-sm mb-3">
        <div class="container">
            <a class="navbar-brand" href="index.php"><img src="img/logo.jpg" width="100"><strong> Entrega+</strong></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="nav justify-content-center">
                <div class="align-self-end">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        <a class="btn btn-light" href="inicio.php" role="button">Voltar</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <h2>Política de Privacidade</h2>
        <br>
        <p>A sua privacidade é importante para nós. É política do Entrega + respeitar a sua privacidade em relação a
            qualquer informação sua que possamos coletar no site <a href="www.entrega+.com">Entrega +</a>, e outros
            sites que possuímos e operamos.</p>
        <br>
        <p>Solicitamos informações pessoais apenas quando realmente precisamos delas para lhe fornecer um serviço.
            Fazemo-lo por meios justos e legais, com o seu conhecimento e consentimento. Também informamos por que
            estamos coletando e como será usado.</p>
        <br>
        <p>Apenas retemos as informações coletadas pelo tempo necessário para fornecer o serviço solicitado. Quando
            armazenamos dados, protegemos dentro de meios comercialmente aceitáveis ​​para evitar perdas e roubos, bem
            como acesso, divulgação, cópia, uso ou modificação não autorizados.</p>
        <br>
        <p>Não compartilhamos informações de identificação pessoal publicamente ou com terceiros, exceto quando exigido
            por lei.</p>
        <br>
        <p>O nosso site pode ter links para sites externos que não são operados por nós. Esteja ciente de que não temos
            controle sobre o conteúdo e práticas desses sites e não podemos aceitar responsabilidade por suas
            respectivas&nbsp;<a href="https://politicaprivacidade.com/" rel="noopener noreferrer" target="_blank"
                style="background-color: transparent; color: rgb(5, 90, 249);">políticas de privacidade</a>.</p>
        <br>
        <p>Você é livre para recusar a nossa solicitação de informações pessoais, entendendo que talvez não possamos
            fornecer alguns dos serviços desejados.</p>
        <br>
        <p>O uso continuado de nosso site será considerado como aceitação de nossas práticas em torno de privacidade e
            informações pessoais. Se você tiver alguma dúvida sobre como lidamos com dados do usuário e informações
            pessoais, entre em contacto connosco.</p>
        <br>
        <p>
        <ul>
            <li>O serviço Google AdSense que usamos para veicular publicidade usa um cookie DoubleClick para veicular
                anúncios mais relevantes em toda a Web e limitar o número de vezes que um determinado anúncio é exibido
                para você.</li>
        <br>
            <li>Para mais informações sobre o Google AdSense, consulte as FAQs oficiais sobre privacidade do Google
                AdSense.</li>
        <br>
            <li>Utilizamos anúncios para compensar os custos de funcionamento deste site e fornecer financiamento para
                futuros desenvolvimentos. Os cookies de publicidade comportamental usados ​​por este site foram
                projetados para garantir que você forneça os anúncios mais relevantes sempre que possível, rastreando
                anonimamente seus interesses e apresentando coisas semelhantes que possam ser do seu interesse.</li>
        <br>
            <li>Vários parceiros anunciam em nosso nome e os cookies de rastreamento de afiliados simplesmente nos
                permitem ver se nossos clientes acessaram o site através de um dos sites de nossos parceiros, para que
                possamos creditá-los adequadamente e, quando aplicável, permitir que nossos parceiros afiliados ofereçam
                qualquer promoção que pode fornecê-lo para fazer uma compra.</li>
        </ul>
        <p><br></p>
        </p>
        <h3>Compromisso do Usuário</h3>
        <br>
        <p>O usuário se compromete a fazer uso adequado dos conteúdos e da informação que o Entrega + oferece no site e
            com caráter enunciativo, mas não limitativo:</p>
        <br>
        <ul>
            <li>A) Não se envolver em atividades que sejam ilegais ou contrárias à boa fé a à ordem pública;</li>
            <li>B) Não difundir propaganda ou conteúdo de natureza racista, xenofóbica, ERROR ou azar, qualquer tipo de
                pornografia ilegal, de apologia ao terrorismo ou contra os direitos humanos;</li>
            <li>C) Não causar danos aos sistemas físicos (hardwares) e lógicos (softwares) do Entrega +, de seus
                fornecedores ou terceiros, para introduzir ou disseminar vírus informáticos ou quaisquer outros sistemas
                de hardware ou software que sejam capazes de causar danos anteriormente mencionados.</li>
        </ul>
        <br>
        <h3>Mais informações</h3>
        <br>
        <p>Esperemos que esteja esclarecido e, como mencionado anteriormente, se houver algo que você não tem certeza se
            precisa ou não, geralmente é mais seguro deixar os cookies ativados, caso interaja com um dos recursos que
            você usa em nosso site.</p>
        
        <p>Esta política é efetiva a partir de&nbsp;<strong>4 November 2022 23:07</strong></p>
        <br>
        <br>
        <br>
        <br>
    </div>
    <footer class="border-top fixed-bottom text-muted bg-light">
        <div class="container">
            <div class="row py-3">
                <div class="col-12 col-md-4 text-center text-md-left">
                    &copy; 2022 - Entrega+
                </div>
                <div class="col-12 col-md-4 text-center">
                    <a href="privacidade.php" class="text-decoration-none text-dark">Política de Privacidade</a>
                </div>
                <div class="col-12 col-md-4 text-center text-md-right">
                <a href="termos.php" class="text-decoration-none text-dark">Termos de Uso</a>
                </div>
            </div>
        </div>
    </footer>


</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</html>