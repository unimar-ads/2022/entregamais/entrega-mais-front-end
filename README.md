# Entrega Mais Front End



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/unimar-ads/2022/entregamais/entrega-mais-front-end.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/unimar-ads/2022/entregamais/entrega-mais-front-end/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.


## Name
PROJETO ENTREGA+.

## Description
Nosso intuito é garantir as funcionalidades básicas para 
melhoria das condições de trabalho e organização dos profissionaisque que atuam com entregas, cujo objetivo, visa garantir a cooperação entre os usuários. Assim, os entregadores poderão ter mais controle acerca da definição de questões organizacionais da prestação de serviço, de modo a garantir um acerto coletivo em razão das circunstâncias de trabalho, tais como: taxa de entrega, grupos de atuação e rotas.

## Pré-requisitos
Ferramentas:
Laravel PHP, vsCode, PHPmyadmin, mySql e bootstrap.
Linguagens:
PHP, SQL e HTML.

## Instalação
Ainda sem definição.

## Execução
Para execução seguir os comandos nas seguintes etapas no terminal:
Referente à API:
1° php artisan serve;
2° php artisan migrate;

Referente ao cliente:
1° 
    No Linux: php -S 0.0.0.0;
    No Windows: no painel do Xampp iniciar apache e mysql;

Após tais passos, basta criar um usuário na tela de login e fazer o subsequente login.

## Colaboradores
Fernanda Gomes de Melo – RA: 190674-4
Alexandre Augusto Gerdulli Rino Guimarães 191847-4

