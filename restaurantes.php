<?php
session_start();
include('verifica_login.php');

//----get
if ($_SERVER["REQUEST_METHOD"] == "GET") {


$api = curl_init();
curl_setopt($api, CURLOPT_URL, "http://localhost:8000/api/restaurantes");
curl_setopt($api, CURLOPT_RETURNTRANSFER, true);
$api_response = json_decode(curl_exec($api));
curl_close($api);

}
//----post

if ($_SERVER["REQUEST_METHOD"] == "POST") {


  $dados = [
      "nome" => $_REQUEST['nome'],
      "cnpj" => $_REQUEST['cnpj'],
      "telefone" => $_REQUEST['telefone'],
      "endereco" => $_REQUEST['endereco']
  ];
  
  $api = curl_init();
  curl_setopt($api, CURLOPT_URL, "http://localhost:8000/api/restaurante");
  curl_setopt($api, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($api, CURLOPT_POST, true);
  curl_setopt($api, CURLOPT_POSTFIELDS, $dados);
  $api_response = curl_exec($api);
  $api_info = curl_getinfo($api);
  curl_close($api);

  $api = curl_init();
  curl_setopt($api, CURLOPT_URL, "http://localhost:8000/api/restaurantes");
  curl_setopt($api, CURLOPT_RETURNTRANSFER, true);
  $api_response = json_decode(curl_exec($api));
  curl_close($api);

}

//----put

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(isset($_REQUEST["idRestaurante"])) {
    $idRestaurante = $_REQUEST["idRestaurante"];
    if($idRestaurante){
      $api = curl_init();
      curl_setopt($api, CURLOPT_URL, "http://localhost:8000/api/restaurante/" . $idRestaurante);
      curl_setopt($api, CURLOPT_RETURNTRANSFER, true);
      $api_response = json_decode(curl_exec($api));
      //var_dump("api_response: ",$api_response);
      curl_close($api);
    }


    $dados = [
      "nome" => $_POST['nome'],
      "cnpj" => $_POST['cnpj'],
      "telefone" => $_POST['telefone'],
      "endereco" => $_POST['endereco']
    ];


    $api = curl_init();
    $id = $idRestaurante;
    curl_setopt($api, CURLOPT_URL, "http://localhost:8000/api/restaurante/" . $idRestaurante);
    curl_setopt($api, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($api, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($api, CURLOPT_POSTFIELDS, http_build_query($dados));
    $api_response = curl_exec($api);
    $api_info = curl_getinfo($api);
    //print_r("http://localhost:8000/api/restaurante/" . $idRestaurante);
    curl_close($api);

    $api = curl_init();
    curl_setopt($api, CURLOPT_URL, "http://localhost:8000/api/restaurantes");
    curl_setopt($api, CURLOPT_RETURNTRANSFER, true);
    $api_response = json_decode(curl_exec($api));
    curl_close($api);
  }
}


//----delete

if (empty($_REQUEST['btnExcluir']) && isset($_REQUEST["idRestaurante"])) {

  $api = curl_init();
  curl_setopt($api, CURLOPT_URL, "http://localhost:8000/api/restaurante/" . $idRestaurante);
  curl_setopt($api, CURLOPT_CUSTOMREQUEST, "DELETE");
  $result = curl_exec($api);
  $httpCode = curl_getinfo($api, CURLINFO_HTTP_CODE);
  curl_close($api);
  return $result;
 
  

}


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="estilo.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Entrega+</title>


</head>


<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-danger border-bottom shadow-sm mb-3">
        <div class="container">
            <a class="navbar-brand" href="index.php"><strong>Entrega+</strong></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="nav justify-content-center">
                <div class="align-self-end">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        <a class="btn btn-light" href="inicio.php" role="button">Voltar</a>
                        <a class="btn btn-light" href="logout.php" role="button">Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#cadastroRestaurante">Cadastro de Restaurante</button>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">Nome</th>
        <th scope="col">CNPJ</th>
        <th scope="col">Telefone</th>
        <th scope="col">Endereço</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($api_response as $restaurantes) : ?>
        <tr>
          <td><?php echo $restaurantes->nome; ?></td>
          <td><?php echo $restaurantes->cnpj; ?></td>
          <td><?php echo $restaurantes->telefone; ?></td>
          <td><?php echo $restaurantes->endereco; ?></td>
          <td type="button" class="btn btn-secondary" id="btnEdit"><a data-toggle="modal" data-target="#editarRestaurante-<?php echo $restaurantes->id ?>">Editar</a></td>
          <td type="button" class="btn btn-secondary" id="btnExcluir"><a data-toggle="modal" data-target="#editarRestaurante-<?php echo $restaurantes->id ?>">Excluir</a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <!-- Modal Cadastro -->
  <div class="modal fade" id="cadastroRestaurante" tabindex="-1" role="dialog" aria-labelledby="cadastroRestauranteLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="cadastroRestauranteLabel">Cadastro Restaurante</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <div class="modal-body">
            <div class="mb-3">
              <label for="nome">Nome</label>
              <input id="nome" name="nome" class="form-control" type="text" required />
            </div>
            <div class="mb-3">
              <label for="cnpj">CNPJ</label>
              <input id="cnpj" name="cnpj" class="form-control" type="text" required />
            </div>
            <div class="mb-3">
              <label for="tel">Telefone</label>
              <input id="telefone" name="telefone" class="form-control" type="text" required />
            </div>
            <div class="mb-3">
              <label for="endereco">Endereço</label>
              <input id="endereco" name="endereco" class="form-control" type="text" required />
            </div>
        </div>
        <div class="modal-footer">            
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Salvar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <!-- Modal Editar-->
  <?php foreach ($api_response as $restaurantes) : ?>
  <div class="modal fade" id="editarRestaurante-<?php echo $restaurantes->id; ?>" tabindex="-1" role="dialog" aria-labelledby="editarRestauranteLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editarRestauranteLabel">Editar Restaurante</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <div class="modal-body">
            <input id="idRestaurante" name="idRestaurante" type="hidden" required value="<?php echo $restaurantes->id; ?>"/>
            <div class="mb-3">
              <label for="nome">Nome</label>
              <input id="nome" name="nome" class="form-control" type="text" required value="<?php echo $restaurantes->nome; ?>"/>
            </div>
            <div class="mb-3">
              <label for="cnpj">CNPJ</label>
              <input id="cnpj" name="cnpj" class="form-control" type="text" required value="<?php echo $restaurantes->cnpj; ?>"/>
            </div>
            <div class="mb-3">
              <label for="tel">Telefone</label>
              <input id="telefone" name="telefone" class="form-control" type="text" required value="<?php echo $restaurantes->telefone; ?>" />
            </div>
            <div class="mb-3">
              <label for="endereco">Endereço</label>
              <input id="endereco" name="endereco" class="form-control" type="text" required value="<?php echo $restaurantes->endereco; ?>" />
            </div>
          </div>
          <div class="modal-footer">            
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Salvar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <?php endforeach; ?>

</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


</html>
